
import Head from 'next/head'


export default function Player() {
    return ( <>

        <Head>
          {/* <title>My Yoto {cardTitle}</title> */}
          <title>My Yoto</title>

          <meta property="og:description" content="A carefully connected screen-free speaker.Made for children, controlled with physical cards and playing only the audio content you want them to listen to." />
          <meta property="og:title" content="MyApp" />
          <meta property="og:image" content='/img/MYO.png' />

          {/* Primary Meta Tags */}
          <meta name="title" content="My Yoto" />
          <meta name="description" content="A carefully connected screen-free speaker. Made for children, controlled with physical cards and playing only the audio content you want them to listen to. No camera. No microphone. No Ads." />

          {/* Open Graph / Facebook */}
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://my.yotoplay.com/play?cardUrl=https://api.yotoplay.com/card/resolve/brains-on"/>
          <meta property="og:title" content="My Yoto" />
          <meta property="og:description" content="A carefully connected screen-free speaker. Made for children, controlled with physical cards and playing only the audio content you want them to listen to. No camera. No microphone. No Ads." />
          <meta property="og:image" content='/img/MYO.png' />

          {/* Twitter */}
          <meta property="twitter:card" content="summary_large_image"/>
          <meta property="twitter:url" content="https://my.yotoplay.com/play?cardUrl=https://api.yotoplay.com/card/resolve/brains-on" />
          <meta property="twitter:title" content="My Yoto"/>
          <meta property="twitter:description" content="A carefully connected screen-free speaker. Made for children, controlled with physical cards and playing only the audio content you want them to listen to. No camera. No microphone. No Ads."/>
          <meta property="twitter:image" content='/img/MYO.png' />

        </Head>
          <h1>Player Page</h1>

    
    </>
    )
  
    
  }

 